package com.classpath.consumer.client;

import com.classpath.consumer.config.ApplicationConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

public class ConsumerClient {
    public static void main(String[] args) throws InterruptedException {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, ApplicationConfig.BOOTSTRAP_SERVER);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, ApplicationConfig.GROUP_ID);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        KafkaConsumer<Integer, String> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Arrays.asList(ApplicationConfig.TOPIC));
        boolean flag = true;
        while (flag){
            //time taken to process the message
            Thread.sleep(3000);
            ConsumerRecords<Integer, String> consumerRecords = kafkaConsumer.poll(1000);
            Iterator<ConsumerRecord<Integer, String>> iterator = consumerRecords.iterator();
            while(iterator.hasNext()){
                ConsumerRecord<Integer, String> consumerRecord = iterator.next();
                Integer key = consumerRecord.key();
                String value = consumerRecord.value();
                int partition = consumerRecord.partition();
                System.out.println(" Message : Key "+ key + " Message: value "+ value + "partition :: "+ partition);
            }
        }
        kafkaConsumer.close();
    }
}
