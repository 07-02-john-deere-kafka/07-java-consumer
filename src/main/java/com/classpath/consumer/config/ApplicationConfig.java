package com.classpath.consumer.config;

public class ApplicationConfig {
    public static final String GROUP_ID="common_cg_id";
    public static final String APP_ID="producer-app";
    public static final String BOOTSTRAP_SERVER="52.66.6.115:9092";
    public final static String TOPIC = "messages-topic";
}
